﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(gighub5.Startup))]
namespace gighub5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
